#Stat WebSocket for 'Accounting' project.

###Deploy:
1. Edit project variables in `env.py.example` and save it as `env.py` (!!!REQUIRED BEFORE BUILDING DOCKER IMAGE!!!)
2. Build docker image:  
`$ sudo docker build -t accounting-ws . --force-rm`
3. Run container:  
`$ sudo docker run -d -v "/var/log/accounting-ws/:/accounting-ws/log/" -p 3333:3333 -t accounting-ws`
4. Knock-knock to `http://<ifconfig:docker0>:3333/stat/`
5. PROFIT!

###Access to host database from container (docker -> mysql-host):
1. `sudo nano mysqld.cnf` on host machine
2. `bind-address = localhost` -> `bind-address = 0.0.0.0`
3. Create new user for docker instance, for example:  
`CREATE USER 'accounting-ws'@'%' IDENTIFIED BY 'password' PASSWORD EXPIRED;`
4. Then grant all privileges to the new user:  
``GRANT ALL PRIVILEGES ON `my_db`.* TO accounting-ws@'%' IDENTIFIED BY 'password';``
5. Test from container:  
`mysql -h <ifconfig:enp2s0> -u accounting-ws -p <my_db>`

###For `npm` fappers, kek:
1. `npm run build`
2. `npm run start`
3. If you want to stop container: `npm run stop`
4. If you want to rebuild new container: `npm run clean && npm run build`