# Version: 0.0.4
FROM ubuntu

MAINTAINER Pavel Pavlov <p.pavlov@cronix.ms>

WORKDIR /
# update repos
RUN apt-get update

# Install default tools
RUN apt-get install -y tar git curl nano wget dialog net-tools build-essential tcl sed \
    python python-dev python-setuptools python-pip \
    python3 python3-dev python3-setuptools python3-pip\
    supervisor

# Install redis
RUN curl -O http://download.redis.io/redis-stable.tar.gz && tar xzvf redis-stable.tar.gz && rm -rf *.tar.gz
RUN make -C redis-stable/ -j && make -C redis-stable/ -j install

# Setting up redis
RUN mkdir /etc/redis/ && cp /redis-stable/redis.conf /etc/redis/
RUN sed -i 's/ dir .\/ / dir \/var\/lib\/redis /g' /etc/redis/redis.conf \
    && sed -i 's/daemonize no/daemonize yes/g' /etc/redis/redis.conf

RUN adduser --system --group --no-create-home redis
RUN mkdir /var/lib/redis \
    && chown redis:redis /var/lib/redis \
    && chmod 770 /var/lib/redis

# Deploy project
ADD . /accounting-ws
RUN pip3 install -r /accounting-ws/requirements.txt

# Setting up configs
RUN mkdir /etc/supervisord && cp /accounting-ws/conf.d/supervisor/app.ini.example /etc/supervisord/accounting-ws.ini \
    && cp /accounting-ws/conf.d/supervisor/supervisord.conf.example /etc/supervisord.conf

# Run service
CMD ["supervisord", "-c", "/etc/supervisord.conf"]

# Bind to port 3333, 3306
EXPOSE 3333
EXPOSE 3306
