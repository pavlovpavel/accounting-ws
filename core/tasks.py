from celery import Celery
from collections import Counter
import pymysql
import json
from ast import literal_eval

import env
import core.utils as utils

app = Celery('tasks', broker=env.BROKER_URL, backend=env.RESULT_URL)


def get_previous_stat(user_id, lesson_id):
    conn = pymysql.connect(host=env.DB_HOST,
                           user=env.DB_USER,
                           password=env.DB_PASSWORD,
                           db=env.DB_DATABASE,
                           charset=env.DB_CHARSET,
                           cursorclass=pymysql.cursors.DictCursor)
    try:
        with conn.cursor() as cur:

            query = ("SELECT `*` "
                     "  FROM `lesson_user` "
                     "    WHERE `user_id`='{}' "
                     "    AND `lesson_id` = '{}'".format(user_id, lesson_id))

            cur.execute(query)
            try:
                stat = cur.fetchone()
            except Exception:
                stat = None

            # get rid of empty strings and zeros
            for key, value in stat.items():
                if value == '' or value == 0.0 or value == 0:
                    value = None
                stat[key] = value

            return stat
    finally:
        conn.close()


def parse_new_stats(user_id, lesson_id, secs, speed, entity, lesson_re):
    stat = dict()

    stat['user_id'] = user_id
    stat['lesson_id'] = lesson_id

    stat['interval'] = secs
    stat['interval_re'] = lesson_re

    # calc speed
    speed = sum(speed) / len(speed)
    stat['speed'] = speed

    stat['entity'] = entity

    return stat


def merge_old_and_new_stats(stat_new, stat_old):
    stat_final = dict()

    # unpack new stats
    user_id = stat_new['user_id']
    lesson_id = stat_new['lesson_id']
    intervals_new = stat_new['interval']
    intervals_re_new = stat_new['interval_re']
    speed_new = stat_new['speed']
    entity_new = stat_new['entity']

    # if old stat is exist - merge new and old stat
    if stat_old:
        # INTERVALS
        if stat_old['intervals']:
            intervals_old = utils.str_to_list(stat_old['intervals'])
            intervals_new = sorted(list(set(intervals_old + list(intervals_new))))
        if stat_old['intervals_re']:
            intervals_re_old = utils.str_to_list(stat_old['intervals_re'])
            if intervals_re_old:
                intervals_re_new = sorted(list(set(intervals_re_old + list(intervals_re_new))))

        intervals_final = utils.concat_list(sorted(list(intervals_new)))
        intervals_re_final = utils.concat_list(sorted(list(intervals_re_new)))

        # SPEED
        speed_old = stat_old['speed']
        if speed_old:
            speed_final = sum([speed_old, speed_new]) / 2  # always 2 items (old/new speed)
        else:
            speed_final = speed_new

        # ENTITIES
        entity_final = dict()
        entity_old = literal_eval(stat_old['viewed']) if stat_old['viewed'] else None

        if entity_old:
            for key in entity_new.keys():
                if key in entity_old:
                    value1 = Counter(entity_old[key])
                    value2 = Counter(entity_new[key])
                    value_sum = value1 + value2
                    entity_final[key] = dict(value_sum)
                else:
                    entity_final[key] = entity_new[key]
        else:
            entity_final = entity_new

    # else (!old_stat) - create new stat record
    else:
        intervals_final = utils.concat_list(intervals_new)
        intervals_re_final = utils.concat_list(intervals_re_new)

        speed_final = speed_new
        entity_final = entity_new
        stat_final['is_new'] = True

    # Pack final stats // key_name == db_column
    stat_final['user_id'] = user_id
    stat_final['lesson_id'] = lesson_id
    stat_final['intervals'] = intervals_final
    stat_final['intervals_re'] = intervals_re_final
    stat_final['speed'] = speed_final
    stat_final['viewed'] = entity_final

    return stat_final


@app.task
def insert_into_db(stat):
    user_id = stat['user_id']
    lesson_id = stat['lesson_id']
    intervals = stat['intervals']
    intervals_re = stat['intervals_re']
    speed = stat['speed']
    viewed = json.dumps(stat['viewed'])

    conn = pymysql.connect(host=env.DB_HOST,
                           user=env.DB_USER,
                           password=env.DB_PASSWORD,
                           db=env.DB_DATABASE,
                           charset=env.DB_CHARSET,
                           cursorclass=pymysql.cursors.DictCursor)
    try:
        if 'is_new' in stat:
            with conn.cursor() as cur:
                values = (
                    user_id,
                    lesson_id,
                    str(intervals),
                    str(intervals_re),
                    speed,
                    str(viewed)
                )
                query = (
                    "INSERT INTO `lesson_user` "
                    "(`user_id`, "
                    "`lesson_id`, "
                    "`intervals`, "
                    "`intervals_re`, "
                    "`speed`, "
                    "`viewed`) "
                    "VALUES (%s, %s, %s, %s, %s, %s)"
                )
                cur.execute(query, values)
                conn.commit()
        else:
            with conn.cursor() as cur:
                values = (
                    str(intervals),
                    str(intervals_re),
                    speed,
                    viewed,
                    user_id,
                    lesson_id
                )
                query = (
                    "UPDATE lesson_user "
                    "  SET intervals='{}', "
                    "      intervals_re='{}', "
                    "      speed={}, "
                    "      viewed='{}' "
                    "WHERE user_id={} AND `lesson_id`={}".format(*values)
                )
                cur.execute(query)
                conn.commit()
    finally:
        conn.close()
