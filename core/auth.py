import pymysql
import env


def login(token):
    conn = pymysql.connect(host=env.DB_HOST,
                           user=env.DB_USER,
                           password=env.DB_PASSWORD,
                           db=env.DB_DATABASE,
                           charset=env.DB_CHARSET,
                           cursorclass=pymysql.cursors.Cursor)
    try:
        with conn.cursor() as cur:
            query = ("SELECT `*` "
                     "  FROM `users` "
                     "    WHERE `api_token`='{}'".format(token))
            cur.execute(query)
            uid, *rest = cur.fetchone()
            if uid:
                return True, uid
            else:
                return False
    finally:
        conn.close()
