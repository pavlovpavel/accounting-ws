import itertools
import ast


def concat_list(list_seconds):
    """
    Creates list of intervals (lists).

        in  <- [x, y, x1, y1, x2, y2, ... n]                // type == list()
        out -> [[x, y], [x1, y1], [x2, y2], ... [n, n]]     // type == list()

    :param list_seconds: list()

    :return: list_final: list()
    """
    list_final = list()
    start = None
    for el in sorted(list_seconds):
        if not start:
            start = el
        if (list_seconds[-1] == el) or (list_seconds[list_seconds.index(el) + 1] - el > 3):
            list_final.append([start, el])
            if len(list_seconds) - 1 != list_seconds.index(el):
                start = list_seconds[list_seconds.index(el) + 1]
                
    return list_final


def str_to_list(string_list):
    """
    Parse `list of lists` (represented as string) into `unpacked list`.

        in  <- '[[x,y], [x1,y1], [x2,y2], ... [n, n]]'    // type == str()
        out -> [x, y, x1, y1, x2, y2, ... n]              // type == list()

    :param: string_list: str()

    :return: list_final list()
    """

    string = ast.literal_eval(string_list)
    string = [list(range(x[0], x[1])) for x in string]
    list_final = sorted(list(itertools.chain.from_iterable(string)))

    return list_final
