from tornado import web, websocket, ioloop
import json

from env import WS_PORT, SSL_OPTIONS
from core.auth import login
from core.tasks import get_previous_stat, parse_new_stats, merge_old_and_new_stats, insert_into_db


class WSHandler(websocket.WebSocketHandler):

    clients = dict()

    def __init__(self, application, request, **kwargs):
        # Store received values in `self`
        super(WSHandler, self).__init__(application, request, **kwargs)
        self.user_id = None
        self.user_token = None

        self.lesson_id = None
        self.lesson_sec = set()
        self.lesson_re_sec = set()
        self.lesson_speed = list()
        self.lesson_entity = dict()

        self.old_stat = None

    def __str__(self):
        return("User_id: {}\n"
               "Lesson_id: {}\n"
               "Positions: {}\n"
               "Speed: {}\n"
               "Entities: {}".format(self.user_id,
                                     self.lesson_id,
                                     self.lesson_sec,
                                     self.lesson_speed,
                                     self.lesson_entity))

    def check_origin(self, origin):
        # Auth new connection
        self.user_token = self.get_argument('api_token', None)
        self.lesson_id = int(self.get_argument('lesson_id', None))

        status, uid = login(self.user_token)
        if status:
            self.user_id = uid
            return True
        else:
            return False

    def open(self):
        self.old_stat = get_previous_stat(self.user_id, self.lesson_id)

        # Add class' instance to shared dict()
        self.clients[self.user_token] = self

    def on_message(self, message):
        # Parse 'message' values (json)
        data = json.loads(message)

        for key, value in data.items():
            if key == 'position':
                if data['is_reviewed'] is True:
                    self.lesson_re_sec.add(int(value))
                self.lesson_sec.add(int(value))
            elif key == 'speed':
                self.lesson_speed.append(float(value))
            elif key == 'is_reviewed':
                pass
            # else entity
            else:
                if data[key] == 'hint':
                    continue
                if data['is_reviewed'] is True:
                    value = 'lesson_re'
                if key not in self.lesson_entity:
                    self.lesson_entity[key] = {value: (1 * float(data['speed']))}
                else:
                    if value not in self.lesson_entity[key]:
                        self.lesson_entity[key].update({value: (1 * float(data['speed']))})
                    else:
                        self.lesson_entity[key][value] += (1 * float(data['speed']))

    def on_close(self):
        old = get_previous_stat(self.user_id, self.lesson_id),
        new = parse_new_stats(self.user_id, self.lesson_id, self.lesson_sec,
                              self.lesson_speed, self.lesson_entity, self.lesson_re_sec)
        res = merge_old_and_new_stats(new, old[0])
        insert_into_db.delay(res)

        # print(self)
        self.clients.pop(self.user_token)
        del self


if __name__ == "__main__":
    app = web.Application([
        (r'/stat', WSHandler),
    ])

    app.listen(WS_PORT, ssl_options=SSL_OPTIONS)
    ioloop.IOLoop.instance().start()
